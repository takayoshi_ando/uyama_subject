$(function(){
  var data  = [
        {msg: "あいこ" , point: 0} ,
        {msg: "あなたの勝ち" , point: 1} ,
        {msg: "あなたの負け" , point: -1},
        {msg: "ゲームオーバー"}
      ];
  var child_box = $('.child_item img');
  var changeImg;
  function startTimer(){
   changeImg = setInterval(function(){
     var l = 1 + Math.floor(Math.random() * 3);
     $('.parent').children('img').attr({src: "./assets/img/M-j_0" + l + ".png" , data: l});
   } , 20);
   return false;
  };
  function stopTimer(){
    clearInterval(changeImg);
  };
  var click_flg = true;
  function game (i){
  $('.str').html('<p>' + data[i].msg + '</p>');
  var current_score = parseInt($('.point_frame').text());
  $('.point_frame').text(current_score + data[i].point);
  };
  function gameOver(){
  click_flg = false;
  $(this).attr('id' , 'child_item-border');
  $('.str').html('<p>' + data[3].msg + '</p>');
  setTimeout(function (){
  child_box.removeAttr('id' ,'child_item-border');
  $('.str p').remove();
  $('.point_frame').text(5);
  click_flg = true;
  } ,2000);
  };
  function remove_box(){
    child_box.removeAttr('id' ,'child_item-border');
    $('.str p').remove();
  };
  $('.start').on('click' , function(){
    if(click_flg){
      click_flg = false;
      remove_box();
      startTimer();
    } else {
      return false;
    }
  });
  child_box.click(function(){
    stopTimer();
    if ((!($('.str p').length)) && click_flg == false){
      $(this).attr('id' , 'child_item-border');
      var parent_hand = $('.parent_img').attr('data');
      var child_hand = $(this).attr('data');
      var result = parent_hand - child_hand;
      if (result === 1 || result === -2) {
        game(1);
        if(parseInt($('.point_frame').text()) >= 5){
          click_flg = true;
        } else {
          gameOver();
        }
      } else if (result === 2 || result === -1){
        game(2);
        if(parseInt($('.point_frame').text()) >= 5){
          click_flg = true;
        } else {
          gameOver();
        }
      } else if (result === 0){
        $('.str').html('<p>' + data[0].msg + '</p>');
        setTimeout(function (){
        remove_box();
        startTimer();
        }, 1000);
        return false;
      }
    }
    return false;
  });
});
