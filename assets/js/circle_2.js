$(function() {
  var i = 0;
  $('.list').mousedown(function(e) {
    var target = $(e.target);
    if (!target.is('li')){
      var point_x = e.pageX;
      var point_y = e.pageY;
      var len = 'no' + i;
      i++;
      var circle = $('<li></li>').appendTo(this).addClass(len);
      $(this).mousemove(function(e){
        var move_x = e.pageX;
        var move_y = e.pageY;
        var radius = Math.pow((Math.pow((move_y - point_y), 2) + Math.pow((move_x - point_x), 2)), 1 / 2);
        $('.' + len).css({
          top: point_y - radius + 'px',
          left: point_x - radius + 'px',
          width: radius * 2 + 'px',
          height: radius * 2 + 'px'
        });
        return false;
      });
    } else {
      return false;
    }
    $(this).mouseup(function(e){
      var last_x = e.pageX;
      var last_y = e.pageY;
      $(this).off('mousemove');
      if(last_x == point_x && last_y == point_y){
        $('.' + len).remove();
//        $(this).off('mousemdown' , 'mouseup');/* 20160905追加  */
      }
      return false;
    });
  });
//  $(this).off('mousemdown' , 'mouseup');/* 20160824追加  */
  $(this).off('mousemdown' , 'mouseup');
  $('.list').on('click' , 'li' , function(){
    $(this).remove();
    return false;
  });
});
