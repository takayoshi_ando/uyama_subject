$(function(){
  var strMsg1 = "あいこ";
  var strMsg2 = "あなたの勝ち";
  var strMsg3 = "あなたの負け";
  var i = 1;
  var changeImg;
  function startTimer(){
   changeImg = setInterval(function(){
     $('.parent').children('img').attr({src: "./assets/img/M-j_0" + i + ".png" , data: i});
     if(i <= 2){ i++;}
     else { i = 1;}
   } , 50);
   return false;
  };
  function stopTimer(){
    clearInterval(changeImg);
  };
  var click_flg = true;
  $('.start').on('click' , function(){
    if(click_flg){
      click_flg = false;
      $('.child_item img').removeAttr('id' ,'child_item-border');
      $('.str p').remove();
      startTimer();
  } else {
   return false;
 }
  });
    $('.child_item img').click(function(){
      stopTimer();
      if (!($('.str p').length)){
        $(this).attr('id' , 'child_item-border');
        var parent_hand = $('.parent_img').attr('data');
        var child_hand = $(this).attr('data');
        var result = parent_hand - child_hand;
        if (result === 1 || result === -2) {
          $('.str').html('<p>' + strMsg2 + '</p>');
          click_flg = true;
        } else if (result === 2 || result === -1){
          $('.str').html('<p>' + strMsg3 + '</p>');
          click_flg = true;
        } else if (result === 0){
          $('.str').html('<p>' + strMsg1 + '</p>');
          setTimeout(function (){
            $('.child_item img').removeAttr('id' ,'child_item-border');
            $('.str p').remove();
            startTimer();
          }, 1000);
          return false;
        }
      }
      return false;
    });
});
/* あいこの時、じゃんけんを続ける命令が未実装 http://9-bb.com/jquery%E3%81%A7%E3%82%AF%E3%83%AA%E3%83%83%E3%82%AFclick%E3%81%A7%E3%81%AE%E3%80%81%E9%80%A3%E6%89%93%E3%82%92%E9%98%B2%E6%AD%A2%E3%81%97%E3%81%A6-%E3%82%A2%E3%83%8B%E3%83%A1%E3%83%BC%E3%82%B7/*/
