$(function() {
  var pushed = false;
  var y = 0;
  var vy = 0;
  $('body').mousedown(function(e) {
    $(this).css('cursor', 'n-resize');
    y = e.clientY;
    vy = 0;
    pushed = true;
    return false;
  }).mousemove(function(e) {
    if (pushed) {
      vy = y - e.clientY;
      y = e.clientY;
      $(window).scrollTop($(window).scrollTop() + vy);
    }
    return false;
  }).mouseup(function() {
    $(this).css('cursor', 'auto');
    pushed = false;
    var id = setInterval(function() {
      var top = $(window).scrollTop();
      $(window).scrollTop(top + vy);
      if (pushed || top == $(window).scrollTop()) {
        clearInterval(id);
      }
    }, 10);
    return false;
  });
});
